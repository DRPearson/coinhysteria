import { observable, action, computed } from 'mobx';
import NewsAggregatorModel from '../../models/NewsAggregatorModel';

class NewsViewModel {
    @observable selectedArticle;
    @observable selectedSymbol;
}

export default new NewsViewModel();