import React, { Component } from 'react';
import NewsFeed from './NewsFeed';
import NewsContent from './NewsContent';

class News extends Component {
    render() {
        return (
			<div className="row">
                <NewsFeed />
                <NewsContent />
            </div>
        );
    }
}

export default News;