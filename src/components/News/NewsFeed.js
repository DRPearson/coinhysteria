import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';
import autobind from 'autobind-decorator'

@inject('UiModel')
@inject('NewsViewModel')
@inject('NewsAggregatorModel')
@observer
class NewsFeed extends Component {

  @autobind
  setSelectedArticle(article) {
    const {UiModel} = this.props;

    UiModel.setSelectedArticle(article);
  }

  renderFeedItems() {
    const {news} = this.props.NewsAggregatorModel;

    if (!news) {
      return;
    }

    return news.map(article => {
      const {title, body} = article;

      return (
        <tr key={ title } className="pointer" onClick={ e => this.setSelectedArticle(article) }>
          <td className="row news-row">
            <div className="col-12 news-summary">
              { title }
            </div>
            <span className="content-icon-wrapper"><i className="fa fa-chevron-right content-icon" aria-hidden="true"></i></span>
          </td>
        </tr>
      )
    }, this);
  }

  render() {
    return (
      <div className="col col-sm-6 col-xs-12 news">
        <div className="row">
          <div className="col-xs-12 w-100 card-header clearfix">
            <h1 className="h5 m-0 float-left">News (else) TICKER</h1>
          </div>
        </div>
        <div className="row">
          <div className="col-xs-12 news-list">
            <table className="table table-striped">
              <tbody>
                { this.renderFeedItems() }
              </tbody>
            </table>
          </div>
        </div>
      </div>
      );
  }
}

export default NewsFeed;