import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';

@inject('UiModel')
@inject('NewsViewModel')
@inject('NewsAggregatorModel')
@observer
class NewsContent extends Component {
  htmlDecode(content) {
    let e = document.createElement('div');
    e.innerHTML = content;
    return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
  }

  renderTitle() {
    if (!this.props.UiModel || !this.props.UiModel.selectedArticle) {
      return;
    }
    const {selectedArticle: {title}} = this.props.UiModel;

    return (
      <h2>{ title }</h2>
      );
  }

  renderContent() {
    if (!this.props.UiModel || !this.props.UiModel.selectedArticle) {
      return;
    }

    let {selectedArticle: {body}} = this.props.UiModel;

    return (
      <div>
        <p dangerouslySetInnerHTML={ { __html: this.htmlDecode(body) } } />
        <p className="text-right">
          <button type="button" className="btn btn-outline-primary">/r/Stellar  <i className="fa fa-external-link" aria-hidden="true"></i></button>
        </p>
      </div>
      );
  }

  render() {
    return (
      <div className="col col-sm-6 col-xs-12 news-rightbar">
        <button type="button" className="d-lg-none content-toggler close">
          <span aria-hidden="true">×</span>
        </button>
        <div className="card card-block news-content">
          { this.renderTitle() }
          { this.renderContent() }
        </div>
      </div>
      );
  }
}

export default NewsContent;