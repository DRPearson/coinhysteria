import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { Badge, Nav, NavItem, NavLink as RsNavLink } from 'reactstrap';
import classNames from 'classnames';
import { inject, observer } from 'mobx-react';
import Autosuggest from 'react-autosuggest';
import { AsyncTypeahead } from 'react-bootstrap-typeahead';

import nav from './_nav';
import SidebarFooter from './../SidebarFooter';
import SidebarForm from './../SidebarForm';
import SidebarHeader from './../SidebarHeader';
import SidebarMinimizer from './../SidebarMinimizer';

import { NAVIGATION_PERMISSIONS, SIDEBAR_LOCATIONS } from '../../constants/RoutingConstants';

@inject('CryptoCompareModel')
@inject('UiModel')
@inject('Auth')
@inject('SidebarViewModel')
@observer
class Sidebar extends Component {

  handleClick(e) {
    e.preventDefault();
    e.target.parentElement.classList.toggle('open');
  }


  coins() {
    const {coinList} = this.props.CryptoCompareModel;

    if (!coinList) {
      return [];
    }

    return coinList;
  }

  setSelectedCoin(selectedCoin) {
    const {UiModel} = this.props;
    const [coin] = selectedCoin;

    UiModel.setSelectedCoin(coin);
  }

  activeRoute(routeName, props) {
    // return this.props.location.pathname.indexOf(routeName) > -1 ? 'nav-item nav-dropdown open' : 'nav-item nav-dropdown';
    return props.location.pathname.indexOf(routeName) > -1 ? 'nav-item nav-dropdown open' : 'nav-item nav-dropdown';

  }

  // todo Sidebar nav secondLevel
  // secondLevelActive(routeName) {
  //   return this.props.location.pathname.indexOf(routeName) > -1 ? "nav nav-second-level collapse in" : "nav nav-second-level collapse";
  // }

  // badge addon to NavItem
  badge = (badge) => {
    if (badge) {
      const classes = classNames(badge.class);
      return (<Badge className={ classes } color={ badge.variant }>
                { badge.text }
              </Badge>)
    }
  };

  // simple wrapper for nav-title item
  wrapper = item => {
    return (item.wrapper && item.wrapper.element ? (React.createElement(item.wrapper.element, item.wrapper.attributes, item.name)) : item.name)
  };

  // nav list section title
  title = (title, key) => {
    const classes = classNames('nav-title', title.class);
    return (<li key={ key } className={ classes }>
              { wrapper(title) } </li>);
  };

  // nav list divider
  divider = (divider, key) => {
    const classes = classNames('divider', divider.class);
    return (<li key={ key } className={ classes }></li>);
  };

  // nav item with nav link
  navItem = (item, key) => {
    const classes = {
      item: classNames(item.class),
      link: classNames('nav-link', item.variant ? `nav-link-${item.variant}` : ''),
      icon: classNames(item.icon)
    };
    return (
    this.navLink(item, key, classes)
    )
  };

  // nav link
  navLink = (item, key, classes) => {
    const {Auth: {isAuthenticated}} = this.props;
    const url = item.url ? item.url : '';

    if (this.isExternal(url)) {
      return (
        <NavItem key={ key } className={ classes.item }>
          <RsNavLink href={ url } className={ classes.link } active>
            <i className={ classes.icon }></i>
            { item.name }
            { this.badge(item.badge) }
          </RsNavLink>
        </NavItem>
      )
    }

    const {SHOW_AUTHENTICATED, SHOW_NON_AUTHENTICATED, SHOW_ALWAYS} = NAVIGATION_PERMISSIONS;

    if (isAuthenticated && [SHOW_AUTHENTICATED, SHOW_ALWAYS].includes(item.permission)) {
      return (
        <NavItem key={ key } className={ classes.item }>
          <NavLink to={ url } className={ classes.link } activeClassName="active">
            <i className={ classes.icon }></i>
            { item.name }
            { this.badge(item.badge) }
          </NavLink>
        </NavItem>
      )
    }

    if (!isAuthenticated && [SHOW_NON_AUTHENTICATED, SHOW_ALWAYS].includes(item.permission)) {
      return (
        <NavItem key={ key } className={ classes.item }>
          <NavLink to={ url } className={ classes.link } activeClassName="active">
            <i className={ classes.icon }></i>
            { item.name }
            { this.badge(item.badge) }
          </NavLink>
        </NavItem>
      )
    }


  };

  // nav dropdown
  navDropdown = (item, key) => {
    const activeRoute = this.activeRoute;

    return (
      <li key={ key } className={ activeRoute(item.url, props) }>
        <a className="nav-link nav-dropdown-toggle" href="#" onClick={ this.handleClick.bind(this) }><i className={ item.icon }></i>{ item.name }</a>
        <ul className="nav-dropdown-items">
          { this.navList(item.children) }
        </ul>
      </li>)
  };

  // nav type
  navType = (item, idx) => item.title ? title(item, idx) :
    item.divider ? divider(item, idx) :
      item.children ? navDropdown(item, idx)
        : this.navItem(item, idx);

  // nav list
  navList = (items) => {
    return items.map((item, index) => this.navType(item, index));
  };

  isExternal = (url) => {
    const link = url ? url.substring(0, 4) : '';
    return link === 'http';
  };

  renderLoginSignupButton() {
    const {Auth: {isAuthenticated}} = this.props;

    if (isAuthenticated) {
      return;
    }

    return (
      <div className="btn-group" role="group">
        <a href="/login" className="btn btn-auth btn-primary">Sign up</a>
        <a href="/login" className="btn btn-auth btn-primary">Log in</a>
      </div>
    )
  }



  render() {
    const {Auth, SidebarViewModel: {suggestions, suggestedFetch}, SidebarViewModel} = this.props;
    const coins = this.coins();
    const {coinListLoading} = this.props.CryptoCompareModel;

    // sidebar-nav root
    return (
      <div className="sidebar">
        <nav className="sidebar-nav">
          <Nav>
            { this.navList(nav.items) }
            { this.renderLoginSignupButton() }
          </Nav>
        </nav>
        <SidebarFooter/>
        <SidebarMinimizer/>
      </div>
    )
  }
}

export default Sidebar;
