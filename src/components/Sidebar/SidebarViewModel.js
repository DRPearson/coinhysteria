import { observable, action, computed } from 'mobx';
import autobind from 'autobind-decorator'
import RealTimeService from '../../services/RealTimeService';
import CryptoCompareModel from '../../models/CryptoCompareModel';

class SidebarViewModel {
    @observable enteredSymbol = '';
    @observable coinPortfolio = [];

    @autobind @action setEnteredSymbol(e) {
        this.enteredSymbol = e.target.value.toUpperCase();
    }

    @autobind addSymbol() {
        RealTimeService.addSymbolSubscription(this.enteredSymbol);
    }

    get suggestions() {
        const { coinList } = CryptoCompareModel;

        return coinList;
    }

    get suggestedFetch() {
        const { coinList } = CryptoCompareModel;

        if (!coinList) {
            return [];
        }

        const suggestions = Object.keys(coinList).map(coin => {
            const name = coinList[coin].Name; 
            const symbol = coinList[coin].Symbol;

            if (symbol.includes(this.enteredSymbol) || name.includes(this.enteredSymbol)) {
                return coin;
            } 
        });

        return suggestions;
    }
}

export default new SidebarViewModel();