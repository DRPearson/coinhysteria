import { NAVIGATION_PERMISSIONS, SIDEBAR_LOCATIONS } from '../../constants/RoutingConstants';

export default {
  items: [
    {
      name: 'News',
      url: '/dashboard',
      permission: NAVIGATION_PERMISSIONS.SHOW_ALWAYS,
      location: SIDEBAR_LOCATIONS.LEFT,
      icon: 'icon-speedometer',
      badge: {
        variant: 'info',
        text: ''
      }
    },
    {
      name: 'Coins',
      url: '/coins',
      permission: NAVIGATION_PERMISSIONS.SHOW_ALWAYS,
      location: SIDEBAR_LOCATIONS.LEFT,
      icon: 'fa fa-btc',
      badge: {
        variant: 'info',
        text: ''
      }
    },
    {
      name: 'Projections',
      url: '/projections',
      permission: NAVIGATION_PERMISSIONS.SHOW_ALWAYS,
      location: SIDEBAR_LOCATIONS.LEFT,
      icon: 'icon-equalizer',
      badge: {
        variant: 'info',
        text: ''
      }
    },
    {
      name: 'About',
      url: '/about',
      permission: NAVIGATION_PERMISSIONS.SHOW_ALWAYS,
      location: SIDEBAR_LOCATIONS.RIGHT,
      icon: 'icon-equalizer',
      badge: {
        variant: 'info',
        text: ''
      }
    },
    {
      name: 'Logout',
      url: '/logout',
      permission: NAVIGATION_PERMISSIONS.SHOW_AUTHENTICATED,
      location: SIDEBAR_LOCATIONS.LEFT,
      icon: 'icon-logout',
      badge: {
        variant: 'info',
        text: ''
      }
    },
    {
      name: 'Settings',
      url: '/settings',
      permission: NAVIGATION_PERMISSIONS.SHOW_AUTHENTICATED,
      location: SIDEBAR_LOCATIONS.RIGHT,
      icon: 'icon-settings',
      badge: {
        variant: 'info',
        text: ''
      }
    }    
  ]
};
