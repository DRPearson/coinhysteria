import React, { Component } from 'react';
import nav from '../Sidebar/_nav';

class Aside extends Component {

	renderNavList() {
		const { items } = nav;

		return items.map((item) => {
			const { name, permission, location, icon } = item;

			return (
				<li className="list-group-item">{name}</li>
			)
		});
	}

	render() {
		return (
			<aside className="aside-menu">
				<ul className="list-group">
					{ this.renderNavList() }
				</ul>
			</aside>
		)
	}
	
}

export default Aside;