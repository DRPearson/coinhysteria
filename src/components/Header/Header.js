import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';
import { Nav, NavItem, NavbarToggler, NavbarBrand,
} from 'reactstrap';
import { AsyncTypeahead } from 'react-bootstrap-typeahead';
import '../../../node_modules/react-bootstrap-typeahead/css/Typeahead.css';
@inject('CryptoCompareModel')
@inject('UiModel')
@observer
class Header extends Component {

  sidebarToggle(e) {
    e.preventDefault();
    document.body.classList.toggle('sidebar-hidden');
  }

  sidebarMinimize(e) {
    e.preventDefault();
    document.body.classList.toggle('sidebar-minimized');
  }

  mobileSidebarToggle(e) {
    e.preventDefault();
    document.body.classList.toggle('sidebar-mobile-show');
  }

  asideToggle(e) {
    e.preventDefault();
    document.body.classList.toggle('aside-menu-hidden');
  }

  coins() {
    const {coinList} = this.props.CryptoCompareModel;

    if (!coinList) {
      return [];
    }

    return coinList;
  }

  setSelectedCoin(selectedCoin) {
    const {UiModel} = this.props;
    const [ coin ] = selectedCoin;

    UiModel.setSelectedCoin(coin);
  }

  render() {
    const coins = this.coins();
    const {coinListLoading} = this.props.CryptoCompareModel;

    return (
      <header className="app-header navbar">
        <NavbarToggler className="navbar-toggler mobile-sidebar-toggler d-lg-none mr-auto" onClick={ this.mobileSidebarToggle }>
          <span className="navbar-toggler-icon"></span>
        </NavbarToggler>
        <a className="navbar-brand" href="#"></a>
        <NavbarToggler className="navbar-toggler sidebar-toggler d-md-down-none" onClick={ this.sidebarToggle }>
          <span className="navbar-toggler-icon"></span>
        </NavbarToggler>
        <form className="form-inline px-4 d-md-down-none coin-search">
          <AsyncTypeahead labelKey="CoinName" filterBy={ ['FullName', 'Name', 'Symbol'] } isLoading={ coinListLoading } onChange={ selected => this.setSelectedCoin(selected) } options={ coins } placeholder={ 'Search coins' }
          />
          <div className="input-group">
            { /* <input type="text" className="form-control" aria-label="Text input with dropdown button" placeholder="Search coins..." /> */ }
            <span className="input-group-btn coin-search-btn">
              <button className="btn" type="button"><i className="fa fa-search"></i></button>
            </span>
          </div>
        </form>
        <ul className="nav navbar-nav ml-auto">
          <li className="nav-item mr-5">
            <div className="text-white notify-label">
              <strong>Notifications</strong>
            </div>
          </li>
          <li className="nav-item">
            <label className="switch switch-text switch-primary switch-notify">
              <input type="checkbox" className="switch-input" checked="" />
              <span className="switch-label" data-on="On" data-off="Off"></span>
              <span className="switch-handle"></span>
            </label>
          </li>
          <NavbarToggler className="navbar-toggler aside-menu-toggler" onClick={ this.asideToggle }>
            <span className="navbar-toggler-icon"></span>
          </NavbarToggler>
        </ul>
      </header>
      );
  }
}

export default Header;
