import React, {Component} from 'react';

class Footer extends Component {
	render() {
		return (
			<footer className="app-footer">
				<span className="ml-auto">
					<div className="btn-group" role="group" aria-label="First group">
						<button type="button" className="btn btn-light"><i className="fa fa-facebook" aria-hidden="true"></i></button>
						<button type="button" className="btn btn-light"><i className="fa fa-twitter" aria-hidden="true"></i></button>
						<button type="button" className="btn btn-light"><i className="fab fa-discord"></i></button>
						<button type="button" className="btn btn-light">Advertise</button>
					</div>
				</span>
				<span className="pull-right">
					<span className="ml-auto">&nbsp;&nbsp;&nbsp;</span>
					<span className="ml-auto">CoinHysteria &copy; 2018.</span>
				</span>
			</footer>
		)
	}
}

export default Footer;
	