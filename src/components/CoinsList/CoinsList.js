import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';

@inject('CoinsListViewModel')
@observer
class CoinsList extends Component {

    renderCoins() {
        const {topTenCoins} = this.props.CoinsListViewModel;

        return topTenCoins.map(coin => {
            const {rank, name, price_usd, market_cap_usd, percent_change_24h} = coin;

            return (
                <tr className="coin-info">
                  <td className="row coin-row">
                    <a href="#" className="col coin-rank text-center">
                      <a href="#">
                        { rank }
                      </a>
                    </a>
                    <a href="#" className="col coin-ticker-name">
                      <a href="#"><img src="btncoin-logo" />
                        { name }
                      </a>
                    </a>
                    <a href="#" className="col coin-price">
                      <a href="#">${ price_usd }</a>
                    </a>
                    <a href="#" className="col coin-mkcap d-none">
                      <a href="#">${ market_cap_usd }</a>
                    </a>
                    <a href="#" className="col coin-change d-none positive">
                      <a href="#">
                        { percent_change_24h }% <i className="fa fa-chevron-circle-up" aria-hidden="true"></i></a>
                    </a>
                    <span className="content-icon-wrapper"><i className="fa fa-chevron-right content-icon" aria-hidden="true"></i></span>
                  </td>
                </tr>
            )
        })
    }

    render() {
        const coins = this.renderCoins();
        return (
            <table className="table table-striped">
              <tbody>
                <tr>
                  <td className="row coin-row">
                    <div className="col coin-rank text-center">Rank</div>
                    <div className="col coin-name">Cryptocurrency</div>
                    <div className="col coin-price">Price</div>
                    <div className="col coin-mkcap d-none">Market Cap</div>
                    <div className="col coin-mkcap d-none">24H Change</div>
                  </td>
                </tr>
                { coins }
              </tbody>
            </table>
        )
    }
}

export default CoinsList;