import { observable, computed, action } from 'mobx';
import ApiService from '../../services/ApiService';

class CoinsListViewModel { 
    constructor() {
        ApiService.fetchTopTenCoins().then(result => this.setTopTenCoins(result.data));
    }

    @observable topTenCoins = [];

    @action setTopTenCoins(result) {
        this.topTenCoins = result;
    }
}

export default new CoinsListViewModel();
