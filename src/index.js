import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { Provider } from 'mobx-react';

// Styles
// Import Font Awesome Icons Set
import 'font-awesome/css/font-awesome.min.css';
// Import Simple Line Icons Set
import 'simple-line-icons/css/simple-line-icons.css';
// Import Main styles for this application
import '../scss/style.scss'
// Temp fix for reactstrap
import '../scss/core/_dropdown-menu-right.scss'
// Containers
import Full from './containers/Full/'

// Stores
import Auth from './services/Auth';
import NewsAggregatorModel from './models/NewsAggregatorModel';
import RealTimeService from './services/RealTimeService';
import SidebarViewModel from './components/Sidebar/SidebarViewModel';
import NewsViewModel from './components/News/NewsViewModel';
import UiModel from './models/UiModel';
import CryptoCompareModel from './models/CryptoCompareModel';
import CoinsListViewModel from './components/CoinsList/CoinsListViewModel';

ReactDOM.render((
  <BrowserRouter>
    <Provider 
      Auth={Auth}
      CoinsListViewModel={CoinsListViewModel}
      CryptoCompareModel={CryptoCompareModel}
      NewsAggregatorModel={NewsAggregatorModel}
      NewsViewModel={NewsViewModel}
      RealTimeService={RealTimeService}
      SidebarViewModel={SidebarViewModel}
      UiModel={UiModel}>
      <Switch>
        <Route path="/" name="Home" component={ Full } />
      </Switch>
    </Provider>
  </BrowserRouter>
  ), document.getElementById('root'));
