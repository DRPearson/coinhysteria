import { observable, action, computed } from 'mobx';
import autobind from 'autobind-decorator'
import io from 'socket.io-client';

import InstrumentModel from '../models/InstrumentModel';
import CryptoCompareModel from '../models/CryptoCompareModel';
import UiModel from '../models/UiModel';

import { CryptoCompareSocket } from '../constants/CryptoCompareConstants';
import { FIELDS } from '../constants/Fields';

class RealTimeService {
    constructor() {
        this.socket = io.connect(CryptoCompareSocket);

        this.initializeSocket();
    }

    /**
     * Checks exisitng instrument models
     * !== existing model -> create new model + apply values
     */
    initializeSocket() {
        this.socket.on("m", (message) => {
            const tradeData = this.unpackData(message);

            this.checkInstruments(tradeData);
        });
    }

    @observable subscriptions = [];
    @observable deletedSubscriptions = [];
    @observable instruments = [];

    @action
    addSymbolSubscription(symbol) {
        if (this.hasSymbol(symbol)) {
            return;
        }

        this.subscriptions.push(this.formatSubString(symbol));
        this.addSubscriptions();
    }

    @action
    deleteSymbolSubscription(symbol) {
        this.removeSubscriptions(symbol);
    }

    @action
    deleteSymbol(symbol) {
        symbol = this.formatSubString(symbol);

        this.subscriptions.remove(symbol);
        this.deletedSubscriptions.push(symbol);
    }

    @autobind
    hasSymbol(symbol) {
        symbol = this.formatSubString(symbol);

        if (this.subscriptions.includes(symbol)) {
            return true;
        }

        return false;
    }

    @computed
    get selectedCoinData() {
        const {selectedCoin} = UiModel;
        const selectedCoinData = this.instruments.filter(coin => coin.FROMSYMBOL === selectedCoin.Symbol.toUpperCase());

        return selectedCoinData;
    }

    formatSubString(symbol) {
        return `5~CCCAGG~${symbol}~USD`;
    }

    addSubscriptions() {
        this.socket.emit('SubAdd', {
            subs: this.subscriptions
        });
    }

    removeSubscriptions(symbol) {
        this.socket.emit('SubRemove', {
            subs: [symbol]
        });

        this.subscriptions.remove(symbol);
    }

    checkInstruments(tradeData) {
        const {FROMSYMBOL} = tradeData;
        const [existingInstrument] = this.instruments.filter(instrument => instrument.FROMSYMBOL === FROMSYMBOL);

        if (!FROMSYMBOL || FROMSYMBOL === 'CCCAGG') {
            return;
        }

        if (existingInstrument) {
            existingInstrument.applyValues(tradeData);

            return;
        }

        const instrument = new InstrumentModel(tradeData);
        this.instruments.push(instrument);
        this.addSubscriptions();
    }

    unpackData(tradeData) {
        const valuesArray = tradeData.split("~");
        const mask = valuesArray[valuesArray.length - 1];
        const maskInt = parseInt(mask, 16);
        const unpacked = {};

        Object.keys(FIELDS).forEach((property, index) => {
            if (FIELDS[property] === 0) {
                unpacked[property] = valuesArray[index];
            } else if (maskInt && FIELDS[property]) {
                unpacked[property] = valuesArray[index];
            }
        });

        return unpacked;
    }
}

export default new RealTimeService();
