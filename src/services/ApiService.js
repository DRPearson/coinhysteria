import axios from 'axios'

import { REDDIT_CRYPTO_API, REDDIT_GENERAL_API, REPLACE_SYMBOL_NAME } from '../constants/RedditConstants';
import { CRYPTO_COMPARE_FULL_DATA_API, CRYPTO_COMPARE_NEWS_API, CRYPTO_COMPARE_GENERAL_NEWS_API } from '../constants/CryptoCompareConstants';
import { CMC_TOP_10_COINS_API } from '../constants/CoinMarketCapConstants';


class ApiService {
    fetchCoinList() {
        return axios.get('http://localhost:4567/cc/coins');
    }

    fetchCoinFullData(symbol) {
        const formattedCall = this.formatApiCall(CRYPTO_COMPARE_FULL_DATA_API, symbol);

        return axios.get(formattedCall);
    }

    fetchCoinFullNews(symbol) {
        const formattedCall = this.formatApiCall(CRYPTO_COMPARE_NEWS_API, symbol);

        return axios.get(formattedCall);
    }

    fetchGeneralFullNews() {
        return axios.get(CRYPTO_COMPARE_GENERAL_NEWS_API);
    }

    fetchSymbolRedditNews(symbol) {
        const formattedCall = this.formatApiCall(REDDIT_CRYPTO_API, symbol);

        return axios.get(formattedCall);
    }

    fetchGeneralRedditNews() {
        return axios.get(REDDIT_GENERAL_API);
    }

    fetchTopTenCoins() {
        return axios.get(CMC_TOP_10_COINS_API);
    }

    formatApiCall(api, symbol) {
        return api.replace(REPLACE_SYMBOL_NAME, symbol);
    }
}

export default new ApiService();
