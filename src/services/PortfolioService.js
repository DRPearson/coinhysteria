import { inject, observer } from 'mobx';

@inject('Auth')
@observer
class PortfolioService {

    fetchPortfolio() {
        // get api to fetch user portfolio
    }

    updatePortfolio(portfolio) {
        // post api to update user portfolio
    }
}

export default new PortfolioService();