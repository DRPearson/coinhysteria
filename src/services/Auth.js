import auth0 from 'auth0-js';
import { observable, action, computed } from 'mobx';
import history from '../history';
import { AUTH_CONFIG } from './auth0-variables';

class Auth {
  constructor() {
    this.login = this.login.bind(this);
    this.logout = this.logout.bind(this);
    this.handleAuthentication = this.handleAuthentication.bind(this);
    this.checkAuthentication = this.checkAuthentication.bind(this);
    this.getProfile = this.getProfile.bind(this);

    this.scheduleRenewal();
    this.checkAuthentication();
  }

  auth0 = new auth0.WebAuth({
    domain: AUTH_CONFIG.domain,
    clientID: AUTH_CONFIG.clientId,
    redirectUri: AUTH_CONFIG.callbackUrl,
    audience: `https://${AUTH_CONFIG.domain}/userinfo`,
    responseType: 'token id_token',
    scope: 'openid'
  });

  @observable userProfile;
  @observable isAuthenticated;
  tokenRenewalTimeout;

  login() {
    this.auth0.authorize();
  }

  logout() {
    // Clear access token and ID token from local storage
    localStorage.removeItem('access_token');
    localStorage.removeItem('id_token');
    localStorage.removeItem('expires_at');
    // navigate to the home route
    history.replace('/');

    clearTimeout(this.tokenRenewalTimeout);
    this.checkAuthentication();

  }

  handleAuthentication() {
    this.auth0.parseHash((err, authResult) => {
      if (authResult && authResult.accessToken && authResult.idToken) {
        this.setSession(authResult);
        history.replace('/');
        this.checkAuthentication();
      } else if (err) {
        history.replace('/');
        alert(`Error: ${err.error}. Check the console for further details.`);
      }
    });
  }

  getAccessToken() {
    const accessToken = localStorage.getItem('access_token');
    if (!accessToken) {
      throw new Error('No access token found');
    }
    return accessToken;
  }

  @action getProfile(cb) {
    const accessToken = this.getAccessToken();
    this.auth0.client.userInfo(accessToken, (err, profile) => {
      if (profile) {
        this.userProfile = profile;
      }
      cb(err, profile);
    });
  }

  @action setSession(authResult) {
    // Set the time that the access token will expire at
    const expiresAt = JSON.stringify(
      authResult.expiresIn * 1000 + new Date().getTime()
    );

    localStorage.setItem('access_token', authResult.accessToken);
    localStorage.setItem('id_token', authResult.idToken);
    localStorage.setItem('expires_at', expiresAt);

    this.isAuthenticated = true;
    // schedule a token renewal
    this.scheduleRenewal();
    // navigate to the home route
    history.replace('/');

  }

  @action checkAuthentication() {
    // Check whether the current time is past the 
    // access token's expiry time
    const expiresAt = JSON.parse(localStorage.getItem('expires_at'));
    const currentTime = new Date().getTime();
    this.isAuthenticated =  currentTime < expiresAt;
    console.log('in auth ' + this.isAuthenticated);
  }

  renewToken() {
    this.auth0.checkSession({}, (err, result) => {
      if (err) {
        console.log(err);
      } else {
        this.setSession(result);
      }
    }
    );
  }

  scheduleRenewal() {
    const expiresAt = JSON.parse(localStorage.getItem('expires_at'));
    const delay = expiresAt - Date.now();
    if (delay > 0) {
      this.tokenRenewalTimeout = setTimeout(() => {
        this.renewToken();
      }, delay);
    }
  }
}

const singleton = new Auth();

export default singleton;