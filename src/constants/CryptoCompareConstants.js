export const CryptoCompareSocket = 'https://streamer.cryptocompare.com/';
export const CRYPTO_COMPARE_NEWS_API = 'https://min-api.cryptocompare.com/data/news/?categories=REPLACE_SYMBOL_NAME';
export const CRYPTO_COMPARE_FULL_DATA_API = "https://min-api.cryptocompare.com/data/pricemultifull?fsyms=REPLACE_SYMBOL_NAME&tsyms=USD";
export const CRYPTO_COMPARE_GENERAL_NEWS_API = "https://min-api.cryptocompare.com/data/news/";
export const REPLACE_SYMBOL_NAME = 'REPLACE_SYMBOL_NAME' 