import ApiService from '../services/ApiService';
import NewsArticleModel from './NewsArticleModel';
import { observable, computed, action, reaction } from 'mobx';
import UiModel from './UiModel';

class NewsAggregatorModel {
    constructor() {
        this.loadGeneralCryptoCompareNews();

        reaction(
            () => UiModel.selectedCoin,
            coin => coin ? this.loadCryptoCompareCoinNews(coin.Symbol) : this.loadGeneralCryptoCompareNews()
        )
    }

    @observable redditNews;
    @observable news;

    @action
    loadGeneralCryptoCompareNews() {
        ApiService.fetchGeneralFullNews().then(
            response => {
                return response.data.map(article => {
                    const {title, body, source, url, guid} = article;

                    return new NewsArticleModel(title, body, source, url, guid);
                });
            }
        ).then(articles => this.news = articles);

    }

    @action
    loadCryptoCompareCoinNews(symbol) {
        ApiService.fetchCoinFullNews(symbol).then(
            response => {
                return response.data.map(article => {
                    const {title, body, source, url, guid } = article;

                    return new NewsArticleModel(title, body, source, url, guid);
                });
            }
        ).then(articles => this.news = articles);

    }

    @action
    loadSymbolRedditNews(symbol) {
        ApiService.fetchSymbolRedditNews(symbol).then(response => {
            return response.data.data.children.map(article => {
                const {title, selftext_html} = article.data;

                return new NewsArticleModel(title, selftext_html);
            });
        })
            .then(articles => this.redditNews = articles);
    }

    @action
    loadGeneralRedditNews() {
        ApiService.fetchGeneralRedditNews()
            .then(response => {
                return response.data.data.children.map(article => {
                    const {title, selftext_html} = article.data;

                    return new NewsArticleModel(title, selftext_html);
                });
            })
            .then(articles => this.redditNews = articles);
    }
}

export default new NewsAggregatorModel();