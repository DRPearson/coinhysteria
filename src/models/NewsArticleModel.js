import { observable, computed, action } from 'mobx';

class NewsArticleModel {
    constructor(title, body, source, url, guid) {
        this.title = title;
        this.body = body;
        this.source = source;
        this.url = url;
        this.guid = guid;
    }

    @observable title;
    @observable body;
    @observable source;
    @observable url;
    @observable guid;
}

export default NewsArticleModel;