import { observable, computed } from 'mobx';
import PortfolioEntryModel from './PortfolioEntryModel';
import PortfolioService from '../services/PortfolioService';

/**
 * PortfolioModel {
 *  item {
 *    instrument: {},
 *    quantity: {},
 *    purchasePrice: {},
 *  }
 * }
 */
class PortfolioModel {
    @observable investments = [];

    addItem(instrumentModel, quantity, purchasePrice) {
        const investment = PortfolioEntryModel.create(instrumentModel, quantity, purchasePrice);
        this.investments.push(investment);
    }

    updatePortfolio() {
        PortfolioService.updatePortfolio(investments);
    }
}

export default new PortfolioModel();