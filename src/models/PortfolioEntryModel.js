class PortfolioEntryModel {
    create(instrumentModel, quantity, purchasePrice) {
        return new PortfolioEntryModel(instrumentModel, quantity, purchasePrice);
    }

    constructor(instrumentModel, quantity, purchasePrice) {
        this.instrumentModel = instrumentModel;
        this.quantity = quantity;
        this.purchasePrice = purchasePrice;
    }
}

export default PortfolioEntryModel;