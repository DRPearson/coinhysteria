import { observable, action, computed } from 'mobx';
import ApiService from '../services/ApiService';

/**
 * Holds Crypto Compare Data
 */
class CryptoCompareModel {

    constructor() {
        ApiService.fetchCoinList().then(response => this.setCoinList(response.data.Data));
    }

    @observable coinList = [];

    @action setCoinList(coins) {
        const coinList = [];

        Object.keys(coins).map(coin => coinList.push(coins[coin]));

        this.coinList = coinList;
    }

    @computed get coinListLoading() {
        return this.coinList.length === 0 ? true : false;
    }
}

export default new CryptoCompareModel();
