import { observable, computed, action, reaction } from 'mobx';
import RealTimeService from '../services/RealTimeService';
import ApiService from '../services/ApiService';


class UiModel {
    @observable selectedCoin = null;
    @observable staticCoinModel = null;
    @observable selectedArticle = null;

    @action
    setSelectedCoin(coin) {
        if (!coin) {
            return;
        }

        // RealTimeService.addSymbolSubscription(coin.Symbol.toUpperCase());
        this.getStaticCoinData(coin);

        this.selectedCoin = coin;
    }

    @action
    setSelectedArticle(article) {
        this.selectedArticle = article;
    }

    @action
    getStaticCoinData(coin) {
        ApiService.fetchCoinFullData(coin.Symbol.toUpperCase())
        .then(data => {
            const staticData = data.data.DISPLAY[coin.Symbol.toUpperCase()].USD;

            this.staticCoinModel = staticData;
        });
    }
}

export default new UiModel();
