import { observable, action, computed } from 'mobx';
import { FIELDS } from '../constants/Fields';

class InstrumentModel {

    constructor(tradeData) {
        this.applyValues(tradeData);
    }

    @observable AVG;
    @observable BID;
    @observable FLAGS;
    @observable FROMSYMBOL;
    @observable HIGH24HOUR;
    @observable LASTMARKET;
    @observable LASTTRADEID;
    @observable LASTVOLUME;
    @observable LASTVOLUMETO;
    @observable LOW24HOUR;
    @observable LOWHOUR;
    @observable MARKET;
    @observable OFFER;
    @observable OPEN24HOUR;
    @observable OPENHOUR;
    @observable PRICE;
    @observable TOSYMBOL;
    @observable TYPE;
    @observable VOLUME24HOUR;
    @observable VOLUME24HOURTO;
    @observable VOLUMEHOUR;
    @observable VOLUMEHOURTO;

    applyValues(tradeData) {
        Object.keys(tradeData).forEach(item => {
            this[item] = tradeData[item] || this[item];
        }, this);
    }
}

export default InstrumentModel;
