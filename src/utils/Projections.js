const currentPrice = (coinMarketCap, currentCoinSupply) => {
    let currentPrice = (coinMarketCap / currentCoinSupply);
    currentPrice = currentPrice.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
    return currentPrice.toLocaleString();
}

const newPriceByMkCapMultipler = (coinMarketCap, currentCoinSupply, userMultiplierAmount) => {
    let newMarketCap = (coinMarketCap * userMultiplierAmount);
    let newPrice = newMarketCap / currentCoinSupply;
    newPrice = newPrice.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
    return newPrice.toLocaleString();
}

const newMkCapByUserPrice = (currentCoinSupply, userPrice) => {
    let newMkCap = (currentCoinSupply * userPrice);
    newMkCap = newMkCap.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
    return newMkCap.toLocaleString();
}

const newPriceMatchingUserCoinMkCap = (currentCoinSupply, userCoinMkCap) => {
    let newPrice = (currentCoinSupply * userCoinMkCap);
    newPrice = newPrice.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
    return newPrice.toLocaleString();
}

export {
    currentPrice,
    newPriceByMkCapMultipler,
    newMkCapByUserPrice,
    newPriceMatchingUserCoinMkCap
};