import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { Link } from 'react-router-dom';

class About extends Component {
  render() {
    return (
    <div className="animated fadeIn">
      <div className="container h-10" id="content">
        <div className="row h-100 mt-3">
          <aside className="col-md-2">
            <div className="mt-5 mb-3" id="sidemenu">
              <ul className="nav flex-md-column flex-row justify-content-between">
                <li className="nav-item"><Link to="/advertise" className="nav-link pl-0" activeClassName="active">Advertise</Link></li>
                <li className="nav-item"><a href="#" className="nav-link active pl-0">About</a></li>
                <li className="nav-item"><a href="#" className="nav-link pl-0">Advertise</a></li>
                <li className="nav-item"><a href="#" className="nav-link pl-0">Discord</a></li>
                <li className="nav-item"><a href="#" className="nav-link pl-0">Contact</a></li>
                <li className="nav-item"><a href="#" className="nav-link pl-0">Terms of Use</a></li>
                <li className="nav-item"><a href="#" className="nav-link pl-0">Privacy Policy</a></li>
              </ul>
            </div>
          </aside>
          <main className="col py-5">
            <div className="row">
              <div className="col">
                <div className="py-3">
                    <h1 className="mb-3">About CoinHysteria</h1>
                    <span className="anchor" id="sec1"></span>
                    <p>
                      CoinHysteria.com is a web application that allows members to track their favorite coins and it's performance. Our application includes a cryptocurrency projection tool, news aggregator, and analytics ideal for traders and cryptocurrency enthusiasts. PRO account allows users to add custom RSS/Atom feeds, Twitter sources, and Reddit sources. More features coming soon.
                    </p>
                    <p>
                      What are you waiting for? Join the hysteria behind cryptocurrencies - coins and tokens alike!
                    </p>
                    <h2>Follow Us</h2>
                    <a href="https://twitter.com/CoinHysteriaX" target="_blank">Twitter</a>
                </div>
              </div>
              <div className="col-lg-3 col-12">
                <div className="card bg-faded border-0 mb-3">
                  <div className="card-body">
                    <h3 className="card-title">Ad</h3>
                    <p className="card-text">Sponsored text ad goes here.
                    </p>
                    <a href="#" className="btn btn-outline-secondary">Learn More</a>
                  </div>
                </div>
                <div className="card bg-faded border-0 mb-3">
                  <div className="card-body">
                    <h3 className="card-title">BTC</h3>
                    <p className="card-text">BTC's 15-min line chart as the background</p>
                  </div>
                </div>
                <div className="card bg-faded border-0 mb-3">
                  <div className="card-body">
                    <h3 className="card-title">ETH</h3>
                    <p className="card-text">ETH's 15-min ine chart as the background</p>
                  </div>
                </div>
                <div className="card bg-faded border-0 mb-3">
                  <div className="card-body">
                    <h3 className="card-title">LTC</h3>
                    <p className="card-text">LTC's 15-min ine chart as the background</p>
                  </div>
                </div>
              </div>
            </div>
          </main>
        </div>
      </div>
    </div>
    )
  }
}

export default About;