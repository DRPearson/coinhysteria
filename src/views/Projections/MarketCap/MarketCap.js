import React, { Component } from 'react';
import ReactTooltip from 'react-tooltip'

class MarketCap extends Component {
	render() {
		const coinData = this.props.coinData;
		const tooltip = "See what your seleced coin's potential price would be when compared to the top coin by market cap.";

		return (
			<div className="card">
				<div className="card-header">
					<h6 className="h6">Market Cap Projections <i className="fa fa-info-circle" data-tip={ tooltip }></i></h6>
					<ReactTooltip place="right" type="info" effect="float"/>
					<div className="card-actions">
						<a href="#" className="btn bg-info add-coin"><i className="fa fa-plus"></i></a>
					</div>
				</div>
				<div className="">
					<table className="table table-responsive-sm table-striped">
					  <thead className="thead-inverse">
							<tr>
								<th>Coin</th>
								<th>Market Cap</th>
								<th>Price</th>
								<th>x1000 Coins</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Bitcoin</td>
								<td>$190,960,070,294</td>
								<td>$11,297.00</td>
								<td>Col C x 1000</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		);
	}
}

export default MarketCap;