import React, { Component } from 'react';
import ReactTooltip from 'react-tooltip'

class CoinPrice extends Component {
	render() {
		const coinData = this.props.coinData;
		const tooltip = "See what market cap your selected coins have to hit by the price you wish to reach.";

		return (
			<div className="card">
				<div className="card-header">
					<h6 className="h6">Coin Price Projections <i className="fa fa-info-circle" data-tip={ tooltip }></i></h6>
					<ReactTooltip place="right" type="info" effect="float"/>
					<div className="card-actions">
						<a href="#" className="btn bg-success add-coin"><i className="fa fa-plus"></i></a>
					</div>
				</div>
				<div className="">
					<table className="table table-responsive-sm table-striped">
					  <thead className="thead-inverse">
							<tr>
								<th>Coin</th>
								<th>Desired Price</th>
								<th>Supply</th>
								<th>New Market Cap</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Bitcoin</td>
								<td>$100.00</td>
								<td>21,00,000</td>
								<td>$2,100,000,000.00</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		);
	}
}

export default CoinPrice;