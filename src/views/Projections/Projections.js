import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';

import CoinDataRow from '../common/CoinDataRow/CoinDataRow';
import MarketCap from './MarketCap/MarketCap';
import CoinPrice from './CoinPrice/CoinPrice';

@inject('UiModel')
@inject('RealTimeService')
@observer
class Projections extends Component {
	renderGeneralProjections() {
		return <div>To use our projection tools, please sign up. You'll be able to project </div>;
	}

	renderMarketCapProjections() {
		const { staticCoinModel } = this.props.UiModel;

		return (
			<div className="col-lg-12 col-sm-12">
				<MarketCap coinData={ staticCoinModel } />
			</div>
		)
	}

	renderCoinPriceProjections() {
		const { staticCoinModel } = this.props.UiModel;

		return (
			<div className="col-lg-12 col-sm-12">
				<CoinPrice coinData={ staticCoinModel } />
			</div>
		)
	}

	render() {
		const { selectedCoin, staticCoinModel } = this.props.UiModel;
		
		if (!staticCoinModel) {
			return (
				<div>Please select a coin</div>
			)
		}

		return (
			<div class="projection-cards">
				<CoinDataRow coinData={ staticCoinModel } />
				<Tabs>
					<TabList>
						<Tab>Market Cap</Tab>
						<Tab>Coin Price</Tab>
					</TabList>
					<TabPanel>
						{ this.renderMarketCapProjections() }
					</TabPanel>
					<TabPanel>
						{ this.renderCoinPriceProjections() }
					</TabPanel>
				</Tabs>
			</div>
		)
	}
}

export default Projections;