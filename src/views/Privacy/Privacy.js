import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';

class Privacy extends Component {
  render() {
    return (
    <div className="animated fadeIn">
      <div class="container h-10" id="content">
        <div class="row h-100 mt-3">
          <aside class="col-md-2">
            <div class="mt-5 mb-3" id="sidemenu">
              <ul class="nav flex-md-column flex-row justify-content-between">
                <li class="nav-item"><a href="#" class="nav-link active pl-0">About</a></li>
                <li class="nav-item"><a href="#" class="nav-link pl-0">Advertise</a></li>
                <li class="nav-item"><a href="#" class="nav-link pl-0">Discord</a></li>
                <li class="nav-item"><a href="#" class="nav-link pl-0">Contact</a></li>
                <li class="nav-item"><a href="#" class="nav-link pl-0">Terms of Use</a></li>
                <li class="nav-item"><a href="#" class="nav-link pl-0">Privacy Policy</a></li>
              </ul>
            </div>
          </aside>
          <main class="col py-5">
            <div class="row">
              <div class="col">
                <div class="py-3">
                    <h1 class="mb-3">Privacy Policy</h1>
                    
                    <span class="anchor" id="sec1"></span>

                    <p>CoinHysteria ("us", "we", or "our") operates CoinHysteria.com (the "Site"). This page informs you of our policies regarding the collection, use and disclosure of Personal Information we receive from users of the Site.</p>

                    <p>We use your Personal Information only for providing and improving the Site. By using the Site, you agree to the collection and use of information in accordance with this policy. Unless otherwise defined in this Privacy Policy, terms used in this Privacy Policy have the same meanings as in our Terms and Conditions, accessible at CoinHysteria.com.</p>

                    <h3>Collected Data</h3>

                    <p>Like many site operators, we collect information that your browser sends whenever you visit our Site ("Log Data"). This Log Data may include information such as your computer's Internet Protocol ("IP") address, browser type, browser version, the pages of our Site that you visit, the time and date of your visit, the time spent on those pages and other statistics.</p>

                    <h3>Cookies</h3>

                    <p>Cookies are files with small amount of data, which may include an anonymous unique identifier. Cookies are sent to your browser from a web site and stored on your computer's hard drive.</p>

                    <p>Like many sites, we use "cookies" to collect information. You can instruct your browser to refuse all cookies or to indicate when a cookie is being sent. However, if you do not accept cookies, you may not be able to use some portions of our Site.</p>

                    <h3>Information Collection and Use</h3>

                    <p>While using our Site, we may ask you to provide us with certain personally identifiable information that can be used to contact or identify you. Personally identifiable information may include, but is not limited to, your name, email address, postal address and phone number ("Personal Information").</p>

                    <h3>Links to Other Sites</h3>

                    <p>Our Site may contain links to other sites that are not operated by us. If you click on a third party link, you will be directed to that third party's site. We strongly advise you to review the Privacy Policy of every site you visit.</p>

                    <h3>Portfolio Data</h3>

                    <p>CoinHysteria will never share, sell or rent users portfolio data with any third parties.</p>

                    <h3>Security</h3>

                    <p>The security of your Personal Information is important to us, but remember that no method of transmission over the Internet, or method of electronic storage, is 100% secure. While we strive to use commercially acceptable means to protect your Personal Information, we cannot guarantee its absolute security.</p>

                    <p>CoinHysteria has no control over, and assumes no responsibility for, the content, privacy policies, or practices of any third party sites or services.</p>

                    <h3>Changes to this Privacy Policy</h3>

                    <p>CoinHysteria may update this Privacy Policy from time to time. We will notify you of any changes by posting the new Privacy Policy on the Site. You are advised to review this Privacy Policy periodically for any changes.</p>
                </div>
              </div>
              <div class="col-lg-3 col-12">
                <div class="card bg-faded border-0 mb-3">
                  <div class="card-body">
                    <h3 class="card-title">Ad</h3>
                    <p class="card-text">Sponsored text ad goes here.
                    
                    </p>
                    <a href="#" class="btn btn-outline-secondary">Learn More</a>
                  </div>
                </div>
                <div class="card bg-faded border-0 mb-3">
                  <div class="card-body">
                    <h3 class="card-title">BTC</h3>
                    <p class="card-text">BTC's 15-min line chart as the background
                    </p>
                  </div>
                </div>
                <div class="card bg-faded border-0 mb-3">
                  <div class="card-body">
                    <h3 class="card-title">ETH</h3>
                    <p class="card-text">ETH's 15-min ine chart as the background
                    </p>
                  </div>
                </div>
                <div class="card bg-faded border-0 mb-3">
                  <div class="card-body">
                    <h3 class="card-title">LTC</h3>
                    <p class="card-text">LTC's 15-min ine chart as the background
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </main>
        </div>
      </div>
    </div>
    )
  }
}

export default Privacy;