import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';

class Contact extends Component {
  render() {
    return (
    <div className="animated fadeIn">
      <div class="container h-10" id="content">
        <div class="row h-100 mt-3">
          <aside class="col-md-2">
            <div class="mt-5 mb-3" id="sidemenu">
              <ul class="nav flex-md-column flex-row justify-content-between">
                <li class="nav-item"><a href="#" class="nav-link active pl-0">About</a></li>
                <li class="nav-item"><a href="#" class="nav-link pl-0">Advertise</a></li>
                <li class="nav-item"><a href="#" class="nav-link pl-0">Discord</a></li>
                <li class="nav-item"><a href="#" class="nav-link pl-0">Contact</a></li>
                <li class="nav-item"><a href="#" class="nav-link pl-0">Terms of Use</a></li>
                <li class="nav-item"><a href="#" class="nav-link pl-0">Privacy Policy</a></li>
              </ul>
            </div>
          </aside>
          <main class="col py-5">
            <div class="row">
              <div class="col">
                <div class="py-3">
                    <h1 class="mb-3">Contact Us</h1>
                    <span class="anchor" id="sec1"></span>
                    <p>
                      Any feedback including bugs and feature requests are always welcome.
                    </p>
                    <p>
                      You can contact us on Discord or <a href="mailto:support@coinhysteria.com" target="_blank">support@coinhysteria.com</a>.
                    </p>
                </div>
              </div>
              <div class="col-lg-3 col-12">
                <div class="card bg-faded border-0 mb-3">
                  <div class="card-body">
                    <h3 class="card-title">Ad</h3>
                    <p class="card-text">Sponsored text ad goes here.
                    </p>
                    <a href="#" class="btn btn-outline-secondary">Learn More</a>
                  </div>
                </div>
                <div class="card bg-faded border-0 mb-3">
                  <div class="card-body">
                    <h3 class="card-title">BTC</h3>
                    <p class="card-text">BTC's 15-min line chart as the background</p>
                  </div>
                </div>
                <div class="card bg-faded border-0 mb-3">
                  <div class="card-body">
                    <h3 class="card-title">ETH</h3>
                    <p class="card-text">ETH's 15-min ine chart as the background</p>
                  </div>
                </div>
                <div class="card bg-faded border-0 mb-3">
                  <div class="card-body">
                    <h3 class="card-title">LTC</h3>
                    <p class="card-text">LTC's 15-min ine chart as the background</p>
                  </div>
                </div>
              </div>
            </div>
          </main>
        </div>
      </div>
    </div>
    )
  }
}

export default Contact;