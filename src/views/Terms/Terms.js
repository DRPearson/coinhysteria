import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';

class Terms extends Component {
  render() {
    return (
    <div className="animated fadeIn">
      <div class="container h-10" id="content">
        <div class="row h-100 mt-3">
          <aside class="col-md-2">
            <div class="mt-5 mb-3" id="sidemenu">
              <ul class="nav flex-md-column flex-row justify-content-between">
                <li class="nav-item"><a href="#" class="nav-link active pl-0">About</a></li>
                <li class="nav-item"><a href="#" class="nav-link pl-0">Advertise</a></li>
                <li class="nav-item"><a href="#" class="nav-link pl-0">Discord</a></li>
                <li class="nav-item"><a href="#" class="nav-link pl-0">Contact</a></li>
                <li class="nav-item"><a href="#" class="nav-link pl-0">Terms of Use</a></li>
                <li class="nav-item"><a href="#" class="nav-link pl-0">Privacy Policy</a></li>
              </ul>
            </div>
          </aside>
          <main class="col py-5">
            <div class="row">
              <div class="col">
                <div class="py-3">
                    <h1 class="mb-3">Terms of Use</h1>
                    
                    <span class="anchor" id="sec1"></span>

                    <p>
                      Last updated: January 01, 2018
                    </p>

                    <p>
                      By accessing or using the Service you agree to be bound by these Terms. If you disagree with any part of the terms then you may not access the Service.
                    </p>

                    <p>
                      Please read these Terms and Conditions ("Terms", "Terms and Conditions") carefully before using the http://www.coinhysteria.com website (the "Service") operated by CoinHysteria ("us", "we", or "our").
                    </p>

                    <p>
                      Your access to and use of the Service is conditioned on your acceptance of and compliance with these Terms. These Terms apply to all visitors, users and others who access or use the Service.
                    </p>

                    <h3>Exclusions</h3>

                    <p>
                      Some jurisdictions do not allow the exclusion of certain warranties or the exclusion or limitation of liability for consequential or incidental damages, so the limitations above may not apply to you.
                    </p>

                    <h3>Governing Law</h3>

                    <p>
                      These Terms shall be governed and construed in accordance with the laws of Latvia, without regard to its conflict of law provisions and excluding the United Nations Convention on Contracts for the International Sale of Goods (CISG).
                    </p>

                    <p>
                      Our failure to enforce any right or provision of these Terms will not be considered a waiver of those rights. If any provision of these Terms is held to be invalid or unenforceable by a court, the remaining provisions of these Terms will remain in effect. These Terms constitute the entire agreement between us regarding our Service, and supersede and replace any prior agreements we might have between us regarding the Service.
                    </p>

                    <h3>Links to Other Web Sites</h3>

                    <p>
                      Our Service may contain links to third-party web sites or services that are not owned or controlled by CoinHysteria.
                    </p>

                    <p>
                      CoinHysteria has no control over, and assumes no responsibility for, the content, privacy policies, or practices of any third party web sites or services. You further acknowledge and agree that CoinHysteria shall not be responsible or liable, directly or indirectly, for any damage or loss caused or alleged to be caused by or in connection with use of or reliance on any such content, goods or services available on or through any such web sites or services.
                    </p>

                    <p>
                      We strongly advise you to read the terms and conditions and privacy policies of any third-party web sites or services that you visit.
                    </p>

                    <h3>Indemnification</h3>

                    <p>
                      You agree to defend, indemnify and hold harmless CoinHysteria and its licensee and licensors, and their employees, contractors, agents, officers and directors, from and against any and all claims, damages, obligations, losses, liabilities, costs or debt, and expenses (including but not limited to attorney's fees), resulting from or arising out of a) your use and access of the Service, by you or any person using your account and password, or b) a breach of these Terms.
                    </p>

                    <h3>Limitation of Liability</h3>

                    <p>
                      In no event shall CoinHysteria, nor its directors, employees, partners, agents, suppliers, or affiliates, be liable for any indirect, incidental, special, consequential or punitive damages, including without limitation, loss of profits, data, use, goodwill, or other intangible losses, resulting from (i) your access to or use of or inability to access or use the Service; (ii) any conduct or content of any third party on the Service; (iii) any content obtained from the Service; and (iv) unauthorized access, use or alteration of your transmissions or content, whether based on warranty, contract, tort (including negligence) or any other legal theory, whether or not we have been informed of the possibility of such damage, and even if a remedy set forth herein is found to have failed of its essential purpose.
                    </p>

                    <h3>Termination</h3>

                    <p>
                      We may terminate or suspend access to our Service immediately, without prior notice or liability, for any reason whatsoever, including without limitation if you breach the Terms.
                    </p>

                    <p>
                      All provisions of the Terms which by their nature should survive termination shall survive termination, including, without limitation, ownership provisions, warranty disclaimers, indemnity and limitations of liability.
                    </p>

                    <p>
                      We may terminate or suspend your account immediately, without prior notice or liability, for any reason whatsoever, including without limitation if you breach the Terms.
                    </p>

                    <p>
                      Upon termination, your right to use the Service will immediately cease. If you wish to terminate your account, you may simply discontinue using the Service.
                    </p>

                    <p>
                      All provisions of the Terms which by their nature should survive termination shall survive termination, including, without limitation, ownership provisions, warranty disclaimers, indemnity and limitations of liability.
                    </p>

                    <h3>Warranty Disclaimer</h3>

                    <p>
                      Your use of the Service is at your sole risk. The Service is provided on an "AS IS" and "AS AVAILABLE" basis. The Service is provided without warranties of any kind, whether express or implied, including, but not limited to, implied warranties of merchantability, fitness for a particular purpose, non-infringement or course of performance.
                    </p>

                    <p>
                      CoinHysteria its subsidiaries, affiliates, and its licensors do not warrant that a) the Service will function uninterrupted, secure or available at any particular time or location; b) any errors or defects will be corrected; c) the Service is free of viruses or other harmful components; or d) the results of using the Service will meet your requirements.
                    </p>

                    <h3>Changes to these Terms</h3>

                    <p>
                      We reserve the right, at our sole discretion, to modify or replace these Terms at any time. If a revision is material we will try to provide at least 30 days notice prior to any new terms taking effect. What constitutes a material change will be determined at our sole discretion.
                    </p>

                    <p>
                      By continuing to access or use our Service after those revisions become effective, you agree to be bound by the revised terms. If you do not agree to the new terms, please stop using the Service.
                    </p>
                </div>
              </div>
              <div class="col-lg-3 col-12">
                <div class="card bg-faded border-0 mb-3">
                  <div class="card-body">
                    <h3 class="card-title">Ad</h3>
                    <p class="card-text">Sponsored text ad goes here.
                    
                    </p>
                    <a href="#" class="btn btn-outline-secondary">Learn More</a>
                  </div>
                </div>
                <div class="card bg-faded border-0 mb-3">
                  <div class="card-body">
                    <h3 class="card-title">BTC</h3>
                    <p class="card-text">BTC's 15-min line chart as the background
                    </p>
                  </div>
                </div>
                <div class="card bg-faded border-0 mb-3">
                  <div class="card-body">
                    <h3 class="card-title">ETH</h3>
                    <p class="card-text">ETH's 15-min ine chart as the background
                    </p>
                  </div>
                </div>
                <div class="card bg-faded border-0 mb-3">
                  <div class="card-body">
                    <h3 class="card-title">LTC</h3>
                    <p class="card-text">LTC's 15-min ine chart as the background
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </main>
        </div>
      </div>
    </div>
    )
  }
}

export default Terms;