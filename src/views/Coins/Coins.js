import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';

import CoinsList from '../../components/CoinsList/CoinsList';

@inject('Auth')
@inject('UiModel')
@observer
class Coins extends Component {
  // make line 10 a search bar on hover or click 
  renderCards() {
    return (
      <div className="row">
        <div className="col col-sm-6 col-xs-12 coin-rankings">
          <div className="row">
            <div className="col-xs-12 w-100 card-header clearfix">
              <h1 className="h5 m-0 float-left">Coin Rankings</h1>
            </div>
          </div>
          <div className="row">
            <div className="col-xs-12 coins-list">
              <CoinsList />
            </div>
          </div>
        </div>
        <div className="col col-sm-6 col-xs-12 coins-rightbar">
          <button type="button" className="d-lg-none content-toggler close">
            <span aria-hidden="true">×</span>
          </button>
          <div className="card card-block coins-content">
            <h2>Stellar (XLM)</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eget semper risus. Etiam a maximus leo, imperdiet euismod dolor. Phasellus tincidunt ipsum mauris,
              at faucibus lectus posuere id. Mauris pulvinar sagittis risus vel vulputate. In fringilla pretium quam, ut scelerisque lacus molestie a. Praesent iaculis
              quis ex at suscipit. Sed ullamcorper eros sed dui finibus mattis ut et mi. Nam quis massa pretium, placerat turpis ut, elementum massa. Maecenas condimentum
              metus ex, vel blandit enim mollis et. Cras tincidunt hendrerit tincidunt. Sed tincidunt eget sapien nec ullamcorper.</p>
            <p>Maecenas eu justo viverra, viverra mi at, elementum diam. Nunc ut nisi placerat, faucibus neque non, commodo lorem. Donec suscipit sem ante, id gravida ligula
              vestibulum sit amet. Morbi viverra urna ligula, sodales rhoncus urna porta vitae. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per
              inceptos himenaeos. Quisque tempor euismod congue. Nullam condimentum nulla sed nulla gravida, id malesuada nisl iaculis. Mauris arcu mi, efficitur vel neque
              et, faucibus venenatis est. Nulla rhoncus imperdiet ultrices. Curabitur aliquam aliquet tortor sit amet efficitur. Proin imperdiet lacinia libero a sollicitudin.
              Nulla interdum eros aliquet tellus posuere, ullamcorper aliquet leo imperdiet. Nulla elementum placerat risus vel semper.</p>
            <p className="text-right">
              <button type="button" className="btn btn-outline-primary">/r/Stellar  <i className="fa fa-external-link" aria-hidden="true"></i></button>
            </p>
            <div className="bd-example text-center mb-15">
              <img src="http://via.placeholder.com/728x90" className="img-fluid" />
            </div>
            <div className="card">
              <div className="card-header">
                Line Chart
                <div className="card-actions">
                  <a href="http://www.chartjs.org">
                    <small className="text-muted">docs</small>
                  </a>
                </div>
              </div>
              <div className="card-body">
                <div className="chart-wrapper">
                  <canvas id="canvas-1"></canvas>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }

  renderCoinInfo() {
    return <div>coin info</div>;
  }

  render() {
    const {UiModel: {selectedCoin}} = this.props;
    const content = selectedCoin ? this.renderCoinInfo() : this.renderCards();

    return (
      <div className="animated fadeIn">
        { content }
      </div>
      );
  }
}

export default Coins;