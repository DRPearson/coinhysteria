import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';

import News from '../../components/News/News';

@inject('Auth')
@observer
class Dashboard extends Component {
	render() {
		return (
		<div className="animated fadeIn">
			<News />
		</div>
		)
	}
}

export default Dashboard;