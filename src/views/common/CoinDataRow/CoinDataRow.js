import React, { Component } from 'react';
import PropTypes from 'prop-types';

class CoinDataRow extends Component {
    render() {
        const { MKTCAP, PRICE, SUPPLY, CHANGE24HOUR } = this.props.coinData;
        
        return (
          <div className="row coin-data">
            <div className="col-6 col-lg-3">
              <div className="card">
                <div className="card-body p-3 clearfix">
                  <i className="fa fa-usd bg-primary p-3 font-2xl mr-3 float-left coin-data-icon"></i>
                  <div className="text-uppercase text-muted font-weight-bold font-xs text-right mb-0 mt-2">Price</div>
                  <div className="h4 text-right">{ PRICE }</div>
                </div>
                <div className="card-footer px-3 py-2">
                  <a className="font-weight-bold font-xs btn-block text-muted" href="#">View Coin Prices <i className="fa fa-angle-right float-right font-lg coin-data-link"></i></a>
                </div>
              </div>
            </div>
            <div className="col-6 col-lg-3">
              <div className="card">
                <div className="card-body p-3 clearfix">
                  <i className="fa fa-pie-chart bg-primary p-3 font-2xl mr-3 float-left coin-data-icon"></i>
                  <div className="text-uppercase text-muted font-weight-bold font-xs text-right mb-0 mt-2">Market Cap</div>
                  <div className="h4 text-right">{ MKTCAP }</div>
                </div>
                <div className="card-footer px-3 py-2">
                  <a className="font-weight-bold font-xs btn-block text-muted" href="#">View Coin Market Caps <i className="fa fa-angle-right float-right font-lg coin-data-link"></i></a>
                </div>
              </div>
            </div>
            <div className="col-6 col-lg-3">
              <div className="card">
                <div className="card-body p-3 clearfix">
                  <i className="fa fa-bitcoin bg-primary p-3 font-2xl mr-3 float-left coin-data-icon"></i>
                  <div className="text-uppercase text-muted font-weight-bold font-xs text-right mb-0 mt-2">Supply</div>
                  <div className="h4 text-right">{ SUPPLY }</div>
                </div>
                <div className="card-footer px-3 py-2">
                  <a className="font-weight-bold font-xs btn-block text-muted" href="#">View Coin Supply List <i className="fa fa-angle-right float-right font-lg coin-data-link"></i></a>
                </div>
              </div>
            </div>
            <div className="col-6 col-lg-3">
              <div className="card">
                <div className="card-body p-3 clearfix">
                  <i className="fa fa-area-chart bg-primary p-3 font-2xl mr-3 float-left coin-data-icon"></i>
                  <div className="text-uppercase text-muted font-weight-bold font-xs text-right mb-0 mt-2">24hr +/-</div>
                  <div className="h4 text-right">{ CHANGE24HOUR }</div>
                </div>
                <div className="card-footer px-3 py-2">
                  <a className="font-weight-bold font-xs btn-block text-muted" href="#">View Coin 24H Performances <i className="fa fa-angle-right float-right font-lg coin-data-link"></i></a>
                </div>
              </div>
            </div>
          </div>
        );
    }
}

CoinDataRow.propTypes = {

};

export default CoinDataRow;