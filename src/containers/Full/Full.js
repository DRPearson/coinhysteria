import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';
import { Link, Switch, Route, Redirect } from 'react-router-dom';
import { Container } from 'reactstrap';

import Header from '../../components/Header/Header';
import Sidebar from '../../components/Sidebar/Sidebar';
import Breadcrumb from '../../components/Breadcrumb/Breadcrumb';
import Aside from '../../components/Aside/Aside';
import Footer from '../../components/Footer/Footer';
import Callback from '../../components/callback/Callback';

import Dashboard from '../../views/Dashboard/Dashboard';
import About from '../../views/About/About';
import Advertise from '../../views/Advertise/Advertise';
import Coins from '../../views/Coins/Coins';
import Projections from '../../views/Projections/Projections';

@inject('Auth')
@observer
class Full extends Component {

  handleAuthentication({location}) {
    const {Auth} = this.props;

    if (/access_token|id_token|error/.test(location.hash)) {
      Auth.handleAuthentication();
    }
  }

  render() {
    const {Auth} = this.props;

    return (
      <div className="app">
        <Header />
        <div className="app-body">
          <Sidebar {...this.props} />
          <main className="main">
            <Breadcrumb />
            <Container fluid className="main-app">
              <Switch>
                <Route path="/dashboard" name="Dashboard" component={ Dashboard } />
                <Route path="/about" name="About" component={ About } />
                <Route path="/advertise" name="Advertise" component={ Advertise } />
                <Route path="/coins" name="Coins" component={ Coins } />
                <Route path="/projections" name="Projections" component={ Projections } />
                <Route path="/login" render={ () => {
                                                Auth.login();
                                              } } />
                <Route path="/logout" render={ (props) => {
                                                 Auth.logout();
                                                 return <Redirect to="/dashboard" />
                                               } } />
                <Route path="/callback" render={ (props) => {
                                                   this.handleAuthentication(props);
                                                   return <Redirect to="/dashboard" />
                                                 } } />
                <Redirect from="/" to="/dashboard" />
              </Switch>
            </Container>
          </main>
          <Aside />
        </div>
        <Footer />
      </div>
      );
  }
}

export default Full;
