const routes = {
  '/': 'Home',
  '/dashboard': 'Dashboard',
  '/about': 'About',
  '/projections': 'Projections',
  '/advertise': 'Advertise',
  '/privacy-policy': 'Privacy',
  '/terms-conditions': 'Terms',
  '/contact': 'Contact',
  '/login': 'Login',
  '/logout': 'Logout'
};
export default routes;
